describe('Player', () => {
  let player = null

  beforeEach(() => {
    player = new Player()
  })
  
  it('El jugador tiene un máximo de 100 puntos de vida.', () => {
    const maxLife = 100

    expect(player.life()).toEqual(maxLife)
  })

  it('El jugador tiene un máximo de 50 puntos de armadura.',() => {
   const maxArmor = 50

   expect(player.armor()).toEqual(maxArmor)
  })

  it(' El jugador siempre empieza con los puños.',() => {
    const firstWeapon = 'fist'

    expect(player.carryingWeapon()).toEqual(firstWeapon)
  })

  it('El jugador puede empezar con un máximo de cinco armas.',() => {
    const maxArmament = 5

    player.addWeapon('knife')
    player.addWeapon('gun')
    player.addWeapon('shotgun')
    player.addWeapon('machinegun')
    player.addWeapon('revolver')
    player.addWeapon('sword')

    let sizeArmament = player.getArmament().length

    expect(sizeArmament).toEqual(maxArmament)
  })

  it('El jugador no puede tener dos armas iguales.',() =>{
    const armamentResult = ['knife','gun']
    
    player.addWeapon('knife')
    player.addWeapon('gun')
    player.addWeapon('knife')

    expect(player.getArmament()).toEqual(armamentResult)  
  })
  
})