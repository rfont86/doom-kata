MAX_LIFE = 100
MAX_ARMOR = 50
FIRST_WEAPON = 'fist'
MAX_ARMAMENTS = 5


class Player {
  constructor() {
    this.armament = new Set()
    this.weapon = FIRST_WEAPON
 }

  addWeapon(arms){
    if(this._maxCapacityArmament()){
      this.armament.add(arms) 
    }
  }

  life(){
    return MAX_LIFE
  }

  armor(){
    return MAX_ARMOR
  }

  carryingWeapon(){
    return this.weapon
  }

  getArmament() {
    return Array.from(this.armament)
  }

  _maxCapacityArmament(){
    return this.getArmament().length < MAX_ARMAMENTS
  }

}









